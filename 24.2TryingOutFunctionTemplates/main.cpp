#include <iostream>
#include <string>
template <typename T>
T maximum(T a, T b) {
    return (a > b) ? a : b;
}
/*
template <typename T>
T multiply(T a, T b) {
    return a * b;
}
*/
int main() {

    int x{10};
    int y{5};
    auto result = maximum(x, y);
    std::cout << "Result: " << result << std::endl;

    return 0;
}