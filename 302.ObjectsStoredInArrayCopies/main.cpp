#include <iostream>
#include "person.h"


int main(){
    Person p1("John", "Snow",25);
    Person p2("Sam", "Gary",45);
    Person p3("Johny", "dill",5);
    p1.print_info();
    p2.print_info();
    p3.print_info();

    std::cout << "----" << std::endl;

    Person students[] {p1, p2, p3}; //--> chiama tre volte la copia degli oggetti

    for (size_t i = 0; i < std::size(students); i++)
    {
        students[i].print_info();/* code */
    }
    
    std::cout << "----" << std::endl;

    for (Person& p: students)
    {
        p.print_info();/* code */
    }
    

    return 0;
}