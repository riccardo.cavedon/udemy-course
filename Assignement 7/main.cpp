#include <iostream>

using namespace std;

int main()
{
    int dayOfWeek{0};
    int pastDay{0};
    int pastDayOfWeek{0};
    int tmp{0};

    cout << "Which day is today [1: Monday, ..., 7: Sunday]:" << endl;
    cin >> dayOfWeek;

    if( dayOfWeek >= 1 && dayOfWeek <= 7)
    {
        cout << "How many days have passed up to today:" << endl;
        cin >> pastDay;

        switch (dayOfWeek)
        {
            case 1:
                cout << "Today is Monday." << endl;
                break;

            case 2:
                cout << "Today is Tuesday." << endl;
                break;

            case 3:
                cout << "Today is Wednesday." << endl;
                break;

            case 4:
                cout << "Today is Thursday." << endl;
                break;

            case 5:
                cout << "Today is Friday." << endl;
                break;

            case 6:
                cout << "Today is Saturday." << endl;
                break;

            case 7:
                cout << "Today is Sunday." << endl;
                break;

            default:
                break;
        }

        tmp = (pastDay % 7);

        pastDayOfWeek = dayOfWeek - tmp;

        if( pastDayOfWeek < 0 )
        {
            pastDayOfWeek = pastDayOfWeek + 7;
        }

        switch (pastDayOfWeek)
        {
            case 1:
                cout << "If we went "<< pastDay << " days in the apst we would hit a Monday" << endl;
                break;

            case 2:
                cout << "If we went "<< pastDay << " days in the apst we would hit a Tuesday" << endl;
                break;

            case 3:
                cout << "If we went "<< pastDay << " days in the apst we would hit a Wednesday" << endl;
                break;

            case 4:
                cout << "If we went "<< pastDay << " days in the apst we would hit a Thursday" << endl;
                break;

            case 5:
                cout << "If we went "<< pastDay << " days in the apst we would hit a Friday" << endl;
                break;

            case 6:
                cout << "If we went "<< pastDay << " days in the apst we would hit a Saturday" << endl;
                break;

            default:
               cout << "If we went "<< pastDay << " days in the apst we would hit a Sunday" << endl;
                break;
        }

    }
    else
    {
        cout << dayOfWeek << " is not a valid day. Bye!" << endl;
    }


    return 0;
}