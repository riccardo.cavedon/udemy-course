#include <iostream>

using namespace std;

int main() {

    //char *message {"Hello World!"}; //--> da standard non è più permesso la conversione implicita di una stringa costante a puntatore non costante
    const char *message {"Hello World!"}; //OK
    cout << "message: " << message << endl; //Hello world

    //*message = "B"; //compile error also with implict declaration: error: invalid conversion from 'const char*' to 'char' [-fpermissive]
    cout << "*message: " << *message << endl;// H

    //Allow users to modify the string
    char message1[] {"Hello world!"};
    message1[0] ='B';
    cout << "message1: " << message1 << endl; 

    
    return 0;
}
