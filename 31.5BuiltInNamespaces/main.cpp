#include <iostream>
#include <string>

int main(){

    std::cout << "Hello world!" << std::endl;

    std::string message{"Hello world"};
    std::cout << message << std::endl;

    return 0;
}