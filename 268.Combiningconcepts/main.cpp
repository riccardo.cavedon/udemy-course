#include <iostream>
#include <concepts>

template <typename T>
concept TinyType = requires ( T t ) {
    sizeof(T) <= 4; // Check syntax
    requires sizeof(T) <= 4; // Check logic --> nested requirement
};

template <typename T>
//requires std::integral<T> || std::floating_point<T>
//requires std::integral<T> && TinyType<T>
requires std::integral<T> && TinyType<T>
T add(T a, T b) {
    return a + b;
}

int main(){
    long long int x{6};
    long long int y{4};

    add(x, y);

    //std::cout << sizeof(long long int) << std::endl;

    return 0;
}