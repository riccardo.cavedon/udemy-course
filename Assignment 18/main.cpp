#include <iostream>
#include <fstream>
#include <iosfwd>
#include <exception>

bool read_from_file(std::string_view filename, size_t size, int * data)
{
    bool ret = false;
    try {
        std::ifstream fstrem_in{ filename.data(), std::ifstream::in}; // We may fail to open the file
        
        if (fstrem_in.fail()) {
            throw std::runtime_error("Unable to open file");
        }

        int value;
        int index{};
        while (fstrem_in >> value) {
            data[index++] = value;
        }
        
        if (!fstrem_in.eof()) { //Something might go wrong in the process of reading the file.
            throw std::runtime_error("Unable to read file");
        }

        ret = true;
    } catch (const std::runtime_error& ex) {
        std::cout << ex.what() << " " << filename.data() << std::endl;
    }

    return ret;
}

int main(){
    bool ret = false;
    int data[4];
    ret = read_from_file("numbers.txt", sizeof(data), data);

    if( ret == true )
    {
        for( int i : data) {
            std::cout << i << std::endl;
        }
    }

    return 0;
}