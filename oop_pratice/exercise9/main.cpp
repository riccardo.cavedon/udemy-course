/*
Esempio di utilizzo del puntatore this
*/
#include <iostream>

using namespace std;

class Dummy {
    public:
        bool isitime (Dummy& param);
};

bool Dummy::isitime (Dummy& param)
{
    if (&param == this)
        return true;
    else
        return false;
}

int main () {
    Dummy a;
    Dummy* b = &a;
    if (b->isitime(a))
        cout << "yes, &a is b\n";
    
    return 0;
}

/* Ulteriore esempio:
CVector& CVector::operator= (const CVector& param)
{
  x=param.x;
  y=param.y;
  return *this; -->!!
}
*/