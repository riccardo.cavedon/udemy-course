#include <iostream>

using namespace std;

class Vehicle
{
    private:
        int autonomy;
        int litres;

    public:
        Vehicle(int x, int y);
        ~Vehicle();

        void set_autonomy(int x);
        int get_autonomy();
        void set_litres(int x);
        int get_litres();
        float consumption();
};
Vehicle::Vehicle(int x, int y)
{
    autonomy = x;
    litres = y;
}

Vehicle::~Vehicle()
{
    cout << "\nObject destroyed";
}

void Vehicle::set_autonomy(int x)
{
    autonomy = x;
};

int Vehicle::get_autonomy()
{
    return autonomy;
};

void Vehicle::set_litres(int x)
{
    litres = x;
};

int Vehicle::get_litres()
{
    return litres;
};

float Vehicle::consumption()
{
    return static_cast<float>(autonomy)/litres;
}

int main(){
    Vehicle v1(300,40);

    cout << "The vehicle has a consumtion of: " << v1.consumption() << " km/l";

    return 0;
}