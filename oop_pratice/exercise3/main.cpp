#include <iostream>

using namespace std;

class Rectangle
{
    private:
        int base;
        int height;
        int area(){ return height*base; };

    protected:
        float surface; // Shared the surface
        
    public:
        Rectangle(int x, int y) {
            base = x; height = y;
            surface = area();
        };

        int getBase() { return base; };
        int getHeight() { return height; };
        int getArea() { return surface; };
};

class Triangle: public Rectangle
{
    private:
        int height;
        int lenght;
        float area() { return static_cast<float>(lenght*height)/2.0f; }; // Area function rewrited - Overridding

    public:
        Triangle(int la, int al): Rectangle(la, al) {
            lenght = la;
            height = al;
            surface = area();
        };
};

int main() {
    Rectangle r(3, 2);
    cout << "Rectangle area: \n" << r.getArea();

    Triangle tr(5,3);
    cout << "\nTriangle rectangle area: \n" << tr.getArea();
    cout << "\nTriangle height: \n" << tr.getHeight();
    cout << "\nTriangle lenght: \n" << tr.getBase();

    return 0;
}