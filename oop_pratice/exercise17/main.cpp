/*
calsses and default constructors
*/
#include <iostream>
#include <string>
using namespace std;

class Example4 {
    private:
        string* ptr;
    public:
        //Constructors:
        Example4() : ptr(new string){} //--> alloca dinamicamente una stringa e inizializza ptr col puntatore allocazione (stringa vuota)
        Example4 (const string& str) : ptr(new string(str)){} //--> alloca dinamicamente una stringa e inizializza ptr col puntatore allocazione (stringa inizializzata dal costruttore)
        
        //destructor:
        ~Example4 () {delete ptr;}

        // access content:
        const string& content() const {return *ptr;}
};

int main() {
    Example4 foo;
    Example4 bar ("Example");

    cout << "bar's content: " << bar.content() << endl;
    cout << "foo's content: " << foo.content() << endl;

    return 0;
}