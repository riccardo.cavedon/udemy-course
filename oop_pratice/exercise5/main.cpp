// member initialization
#include <iostream>
using namespace std;

class Circle {
    double radius;
  public:
    Circle(double r) : radius(r) { }
    double area() {return radius*radius*3.14159265;}
};

class Cylinder {
    Circle base; //--> Variabile è un oggetto Circle
    double height;
  public:
    //Cylinder(double r, double h) : base (r), height(h) {} // Unico modo per inizializzare base (member initializer list)
    Cylinder(double r, double h) : base {r}, height{h} {} // Stessa cosa di prima

    /*
    Cylinder(double r, double h){
        base(r); --> non andrà mai
        height = h;
    } 
    */
    double volume() {return base.area() * height;}
};

int main () {
  Cylinder foo (10,20);

  cout << "foo's volume: " << foo.volume() << '\n';
  return 0;
}