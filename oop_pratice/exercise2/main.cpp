#include <iostream>

using namespace std;

class Rectangle
{
    private:
        int base;
        int height;
        int area(){ return height*base; };
    
    public:
        Rectangle(int x, int y) { base = x; height = y; };
        int getBase() { return base; };
        int getHeight() { return height; };
        int getArea() { return area(); };
};

class Square : public Rectangle
{
    private:
        int side;
    public:
        Square(int l) : Rectangle(l, l) { side = l; };
        int getSide() { return side; }
};

int main(){
    Rectangle rect(3,2);
    cout << "Rectangle area: " << rect.getArea() << endl;

    Square square(5);
    cout << "Square area: " << square.getArea() << endl;

    return 0;
}