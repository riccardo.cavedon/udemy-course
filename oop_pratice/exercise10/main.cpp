/*
Esempio di utilizzo static
*/
#include <iostream>

using namespace std;

class Dummy
{
    public:
        static int n;
        Dummy () {n++;};
};

int Dummy::n = 0;

int main () {
    Dummy a; // --> n = 1
    Dummy b[5]; // --> n + 5 = 6
    cout << a.n << endl;

    Dummy *c = new Dummy; //--> n + 1 = 7
    cout << Dummy::n << endl;
    delete c;

    return 0;
}