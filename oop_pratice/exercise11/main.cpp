/*
Esempio di utilizzo static
*/
#include <iostream>

using namespace std;

class MyClass {
    public:
        int x;
        MyClass(int val) : x(val){}
        int get() const {return x;}
};

int main() {
    const MyClass foo(10);
    //foo.x = 20;

    cout << foo.get()<< endl;
    return 0;
}