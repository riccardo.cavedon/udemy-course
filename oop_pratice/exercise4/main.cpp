/* Esempi di costruttori */
#include <iostream>

using namespace std;

class Circle {
    double radius;
    public:
        Circle(double r) { radius = r; };        
        double circum() { return 2*radius*3.14159265; }
};   

int main() {
    Circle foo (10.0);
    Circle bar = 10.0;
    Circle baz{10.0};
    Circle qux = {10.0};
    cout << foo.circum() << " functional form\n";
    cout << bar.circum() << " assignment init\n";
    cout << baz.circum() << " uniform init\n";
    cout << qux.circum() << " POD-like\n";


    return 0;
}