/*
calsses and default constructors
*/
#include <iostream>
#include <string>
using namespace std;

class Example3 {
    private:
        string data;
    public:
    Example3 ( const string& str) : data(str) {}
    Example3() {}
    const string& content() const { return data; }//---> const assicura che l'invocazione del metodo  non causerà modifiche all'istanza chiamante
};

int main() {
    Example3 foo;
    Example3 bar ("Example");

    cout << "bar's content: " << bar.content() << endl;

    return 0;
}