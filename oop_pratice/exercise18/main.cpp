/*
calsses and default constructors
*/
#include <iostream>
#include <string>
using namespace std;

class Example5 {
    private:
        string* ptr;
    public:
        Example5 (const string& str) : ptr(new string(str)) {}
        ~Example5 () {delete ptr;}

        //copy constructor
        Example5 (const Example5& x) : ptr(new string(x.content())) {}//--> deep copy
        const string& content() const {return *ptr;}
};

int main() {

    Example5 foo ("Example");
    Example5 bar = foo;

    cout << "bar's content: " << bar.content() << endl;
    cout << "foo's content: " << foo.content() << endl;

    return 0;
}