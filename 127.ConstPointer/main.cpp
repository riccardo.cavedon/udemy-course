#include <iostream>

int main(){
    /*
      //A raw variable that can be modified
	std::cout << std::endl;
    std::cout << "Raw variable that can be modified : " << std::endl;
	
    int number {5};// Not constant, can change number any way we want
    std::cout << "number : " << number << std::endl;
	std::cout << "&number : " << &number << std::endl;
    //Modify
    number = 12;
    number += 7;
    
    //Access - Print out
    std::cout << "number : " << number << std::endl;
	std::cout << "&number : " << &number << std::endl;
    std::cout << std::endl;
    

    int *p_number1 {nullptr};
    int  number1{23};
    
    p_number1 = &number1;
    std::cout << "Pointer and value pointed to : both modifiable : " << std::endl;
    std::cout << "p_number1 :" << p_number1 << std::endl; // Address
    std::cout << "*p_number1 : " << *p_number1 << std::endl; // 23
	std::cout << "number1 : " << number1 << std::endl; // 23
    
    //Change the pointed to value through pointer
	std::cout << std::endl;
    std::cout << "Modifying the value pointed to p_number1 through the pointer : " << std::endl;
    *p_number1 = 432;
	std::cout << "p_number1 : " << p_number1 << std::endl;
    std::cout << "*p_number1  : " << *p_number1 << std::endl;
	std::cout << "number1 : " << number1 << std::endl;

    //Change the pointer itself to make it point somewhere else
	std::cout << std::endl;
	std::cout << "Changing the pointer itself to make it point somewhere else" << std::endl;
    int number2 {56};
    p_number1 = &number2;
    std::cout << "p_number1 :" << p_number1 << std::endl;
    std::cout << "*p_number1  : " << *p_number1 << std::endl;
	std::cout << "number1 : " << number1 << std::endl;
	std::cout << "number2 : " << number2 << std::endl;
    std::cout << std::endl;
    */

    std::cout << "Pointer is modifiable , pointed to value is constant : " << std::endl;
	int number3 {632}; // Although you can omit the const on number3 here and it is still 
                             // going to compile, it is advised to be as explicit as possible in
                             // your code and put the const in front. Make your intents CLEAR.
							 
    const int* p_number3 {&number3}; // Can't modify number3 through p_number3
    
    std::cout << "p_number3 :" << p_number3 << std::endl;
    std::cout << "*p_number3 : " << *p_number3 << std::endl;
	
	std::cout << std::endl;
    std::cout << "Modifying the value pointed to by p_number3 through the pointer (Compile Error) : " << std::endl;
    //*p_number3 = 444; // Compile error 
                      
    //Although we can't change what's pointed to by p_number3,
	//we can still change where it's pointing

	std::cout << std::endl;
    std::cout << "Changing the address pointed to by p_number3 : " << std::endl;
    int number4 {872};
    p_number3 = &number4;
    
    std::cout << "p_number3 :" << p_number3 << std::endl;
    std::cout << "*p_number3 : " << *p_number3 << std::endl;
    
    std::cout << std::endl; 
    return 0;
}