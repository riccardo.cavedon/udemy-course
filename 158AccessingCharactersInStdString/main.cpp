#include <iostream>
#include <cstring>

int main() {
 std::string str2 { "Hello World"};
    
    char * data = str2.data();
    std::cout << "Wrapped c string : " << data << std::endl;
    
    data[0] = 'B';// This also changes std::string.
    
    std::cout << "Wrapped c string (after modification) : " << data << std::endl;
    std::cout << "Original string (after modification ) :" << str2 << std::endl;

    return 0;
}