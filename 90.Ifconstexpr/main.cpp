#include <iostream>

int main(){

    constexpr bool condition {false};
    if constexpr(condition)
    {
        std::cout << "codition is true" << std::endl;
    }
    else
    {
        std::cout << "codition is false" << std::endl;
    }
    return 0;
}