#include <iostream>

using namespace std;

int main()
{
    int num1{0};
    int num2{0};
    int operation = 0;
    int correctResult = 0;
    int response = 0;
    char tryAgain = 'y';
    srand(time(0));

    cout << "Welcome to the greatest calculator on Earth!" << endl;
    
    while (1)
    {
        num1 = rand() % 200;
        num2 = rand() % 200;
        cout << "What's the result of " << num1;

        operation = rand() % 3;
        switch (operation)
        {
        case 0:
            correctResult = num1 + num2;
            cout << " + ";
            break;
        
         case 1:
            correctResult = num1 - num2;
            cout << " - ";
            break;

        default:
            correctResult = num1 * num2;
            cout << " * ";
            break;
        }

        cout << num2 << " : ";
        cin >> response;

        if (response == correctResult)
        {
            cout << "Congratulation! You got the result " << correctResult << " right!" << endl;
        }
        else
        {
            cout << "Naah! The correct result is: " << correctResult << endl;
        }

        cout << endl;
        
        cout << "Do you want me to try again? (Y|N): ";
        cin >> tryAgain;

        if (static_cast<char>(tolower(tryAgain)) == 'n')
        {
            break;
        }

    }
    

    return 0;
}