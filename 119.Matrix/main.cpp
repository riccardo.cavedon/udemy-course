#include <iostream>

using namespace std;

int main(){

  int packages [] [4] {
        {1,2,3,4},
        {5,6,7,8},
        {9,10,11,12},
        {3,4,5,6}
    };
	cout << size(packages)<< endl; // sta il numero di elementi per riga
	cout << sizeof(packages)<< endl; // l'occupazione in memoria dell'array
    for(size_t i{0} ; i < std::size(packages); ++ i){

        for(size_t j{0}; j < std::size(packages[i])  ; ++j){

            std::cout << packages[i][j] << "   ";
        }
        std::cout << std::endl;
    }
}
