#include <iostream>
const double PI {3.14159};

class Cylinder {
// Di default i membri della classe sono definiti private
public:
    double volume() {
        return PI * base_radius * base_radius * height;
    }

    //member variables
private:
    double base_radius{1};
    double height{1};
};



int main(){
    Cylinder cylinder;
    std::cout << "volume : " << cylinder.volume() << std::endl;
    std::cout << "base_radius : " << cylinder.base_radius << std::endl;
    std::cout << "height : " << cylinder.height << std::endl;
    return 0;
}