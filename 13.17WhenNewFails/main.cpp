#include <iostream>

int main(){

    //int *data = new int[1000000000000000000];
    /*
    for (size_t i{0}; i < 10000000; i++)
    {
        try
        {
            int *data = new int[100000000];
        }
        catch (std::exception& ex)
        {
            std::cout << "Somtehing went wrong: " << ex.what() << '\n';
        }
    }*/

    for (size_t i{0}; i < 10000000; i++)
    {
        int *data = new(std::nothrow) int[10000000];
        
        if( data != nullptr)
        {
            std::cout << "Data allocated" << std::endl;
        }
        else
        {
            std::cout << "Data allocated failed" << std::endl;
        }
    }

    std::cout << "Program ok" << std::endl;
    return 0;
}