#include <iostream>

using namespace std;

int main()
{
    int px{0};
    int py{0};

    cout << "Welcome to territory control. Please type in your x and y positions" << endl;
    cout << "Type in your x location: ";
    cin >> px;

    cout << "Type in your y location: ";
    cin >> py;

    if( (px <= 10 && px >= -10)
        && (py <= 5 && py >= -5))
    {
        cout << "You are completely surrounded. Don't move!" << endl;
    }
    else
    {
        cout << "You're out of reach!" << endl;
    }
    
    return 0;
}