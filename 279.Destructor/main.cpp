#include <iostream>
#include <string_view>

class Dog {
    public:
        Dog() = default;
        Dog(std::string_view name_param, std::string_view breed_param, int age_param);
        ~Dog();
    private:
        std::string name;
        std::string breed;
        int *p_age{nullptr};
};

Dog::Dog(std::string_view name_param, std::string_view breed_param, int age_param) {
    name = name_param;
    breed = breed_param;
    p_age = new int;//--> heap allocation
    *p_age = age_param;
    std::cout << "Dog constructor called for : " << name << std::endl;
}

Dog::~Dog() {
    delete p_age; // Prevente leak memory
    std::cout << "Dog destructor called for : " << name << std::endl;
}

/*
void some_func(Dog dog_param) {

}
*/

void some_func2() {
    Dog* p_dog = new Dog("Fluffly", "Shepherd", 2);

    delete p_dog; //--> va ditrutto perché il puntatore è locale e va a morire dopo la chiamata della funzione senza liberare la memoria!!! Attenzione
}

int main(){
    //Dog dog{"Fluffly", "Shepherd", 2};
    //some_func(dog);//--> pass by value (int *p_age{nullptr};) copia l'indirizzo non quello che sta puntando!!!! delete va a cancellare il nulla!! Errore grave
    some_func2();
    std::cout << "Done!" << std::endl;
    return 0;
}