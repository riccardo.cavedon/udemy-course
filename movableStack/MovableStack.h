//
// Created by riccardoca on 24/11/2023.
//

#ifndef MOVABLESTACK_MOVABLESTACK_H
#define MOVABLESTACK_MOVABLESTACK_H


class MovableStack {
public:
    MovableStack();
    ~MovableStack();
    MovableStack(const MovableStack& other);
    MovableStack(MovableStack&& other) noexcept;
    MovableStack& operator=(const MovableStack& other);
    MovableStack& operator=(MovableStack&& other) noexcept;

    void push(int value);
    int pop();
    int top() const;
    bool empty() const;
    int getSize() const;

private:
    int size;
    int capacity;
    int* data;
};


#endif //MOVABLESTACK_MOVABLESTACK_H
