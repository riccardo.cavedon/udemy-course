//
// Created by riccardoca on 24/11/2023.
//

#include "MovableStack.h"
#include "algorithm"

MovableStack::MovableStack() {
    size = 0;
    capacity = 0;
    data = nullptr;
}

MovableStack::~MovableStack() {

}
MovableStack::MovableStack(const MovableStack& other) {
    size = other.size;
    capacity = other.capacity;
    std::copy(other.data, other.data + other.size, data);
}

MovableStack::MovableStack(MovableStack&& other) noexcept {
    if(this == &other) {
        return;
    }

    size = other.size;
    capacity = other.capacity;
    data = other.data;

    // Invalidate
    other.size = 0;
    other.capacity = 0;
    other.data = nullptr;
}

void MovableStack::push(int value) {
    if (size == capacity) {
        int* newData = new int[capacity * 2];
        std::copy(data, data + size, newData);
        delete[] data;
        data = newData;
        capacity *= 2;
    }
    data[size++] = value;
}

int MovableStack::pop()  {
    int ret = 0;
    if(size > 0) {
        size--;
        ret = data[size];
        data[size] = 0;
    }
    else
    {
        throw 0;
    }

    return ret;
}

bool MovableStack::empty() const {
    return (size <= 0);
}

int MovableStack::getSize() const {
    return size;
}

int MovableStack::top() const {
    return data[size-1];
}