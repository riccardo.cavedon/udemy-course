#include <iostream>
#include "MovableStack.h"

int main() {
    MovableStack stack;
    std::cout << stack.getSize() << std::endl;// Prints 0

    stack.push(1);
    stack.push(2);
    stack.push(3);
    std::cout<< stack.getSize() << std::endl;// Prints 3

    stack.pop();
    std::cout << stack.getSize() << std::endl;// Prints 2

    stack.pop();
    std::cout << stack.getSize() << std::endl;// Prints 1

    stack.pop();
    std::cout << stack.getSize() << std::endl;// Prints 0
    return 0;
}
