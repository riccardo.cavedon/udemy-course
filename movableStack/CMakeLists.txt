cmake_minimum_required(VERSION 3.26)
project(movableStack)

set(CMAKE_CXX_STANDARD 17)

add_executable(movableStack main.cpp
        MovableStack.cpp
        MovableStack.h)
