#pragma once
#include <iostream>
#include <cstring>

namespace CU{

class string{
	friend std::ostream& operator<< (std::ostream& out , const CU::string& s);
	friend string operator+(const string& left, const string& right);
	friend string& operator+=(string& left, const string& right);
	public : 
	 string(const char* data, size_t size = 15 ){
		m_size = size;
		m_data = new char [m_size];
		std::strcpy(m_data,data );
	}

	//Copy constructor
	string(const string& source){
		m_size = source.m_size;
		m_data = new char [m_size]; // We want to do deep copy here.
		std::strcpy(m_data,source.m_data);
	}

	//Copy assignment operator
	string& operator= (const string& right_operand){
		std::cout << "Copy assignment operator called" << std::endl;
		if(this != & right_operand){
			delete[] m_data;
			m_data = new char [right_operand.m_size];
			std::strcpy(m_data,right_operand.m_data);
		}
		return *this;
	}
	
	const char* c_str() const{
		return m_data;
	}

	~string(){
		delete [] m_data;
		m_data = nullptr;
	}

	void clear() {
		delete[] m_data;
		m_data = nullptr;
		m_size = 0;
	}

	void insert( unsigned int index, unsigned int count, char* str) {
		if (index >= m_size) {
			std::cerr << "Index outside range" << std::endl;
		}
		else {
			if( ( m_size - index ) < count)
			{
				unsigned int diff = count - (m_size - index);
				char *tmp = new char[this->m_size + diff];
				strcpy(tmp, this->m_data);
				this->m_size = this->m_size + diff;
				this->clear();
				this->m_data = tmp;
				tmp = nullptr; 
				std::strncpy(&m_data[index], str, count);
			}
			else {
				std::strncpy(&m_data[index], str, count);
			}
		}
	}

	void erase(const char val) {
		size_t i = 0;
		for (i = 0; i < m_size; i++) {
			if(m_data[i] == val) {
				// Attenzione all'ultimo valore
				if( i == (m_size - 1) )
				{
					m_data[i] = '\0';
				}
				else
				{
					strncpy(&m_data[i], &m_data[i + 1], m_size - 1);
				}
			}
		}
	}

	int compare(const string& val) {
		return strcmp(this->c_str(), val.c_str());
	}

	void append(const string& val, size_t lenght) {
		size_t new_size = this->m_size + lenght;
		char *tmp = new char[new_size];
		strcpy(tmp, this->c_str());
		strcpy(&tmp[this->m_size], val.c_str());
		this->clear();
		this->m_size = new_size;
		this->m_data = tmp;
		tmp = nullptr; 
	}

	private : 
	 unsigned int m_size;
	 char * m_data{nullptr};
};

	inline std::ostream& operator<< (std::ostream& out , const CU::string& s){
		out << s.m_data;
		return out;
	}

	inline string operator+(const string& left, const string& right){
		string tmp(left);
		tmp += right;
		return tmp;
	}

	inline string& operator+=(string& left, const string& right){
		std::strcat(left.m_data,right.m_data);
		return left;
	}
}