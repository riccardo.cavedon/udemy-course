#include <iostream>
/*
NOO l'uscita dipende dall'ordine dei parametri
template <typename T, typename P>
T maximum(T a, P b) {
    return ((a > b) ? a: b);
}
*/
/* Prova 2
template <typename ReturnType, typename T, typename P>
ReturnType maximum(T a, P b) {
    return ((a > b) ? a: b);
}*/

/* Prova 3
template <typename T,typename ReturnType, typename P>
ReturnType maximum(T a, P b) {
    return ((a > b) ? a: b);
}*/

/* Prova 4 */
template <typename T, typename P, typename ReturnType>
ReturnType maximum(T a, P b) {
    return ((a > b) ? a: b);
}

int main(){

    int a{5};
    double b{6.7};

    //auto result = maximum(a,b); //--> così il compilatore non riesce a dedurre il valore di ritorno
    //auto result = maximum<int,double,double>(a,b); //->OK (Prova 2)

    //auto result = maximum<int,double>(a,b); //->OK (Prova 3)
    //auto result = maximum<int>(a,b); //->Fail perché non è specificato il tipo di ritorno (Prova 3)

    //auto result = maximum<int,double>(a,b); //->OK (Prova 4)
    auto result = maximum<int,double>(a,b); //->->Fail perché non è specificato il tipo di ritorno (Prova 4)

    std::cout << "sizeof(result) : " << sizeof(result) << std::endl;
    return 0;
}