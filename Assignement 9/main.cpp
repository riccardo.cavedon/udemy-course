#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    const unsigned int SPACING = 8;
    unsigned int year{0};
    unsigned int weekDayInitMonth{0};

    cout << "Enter a year:" << endl;
    cin >> year;

    cout << "Enter the first day of the year [1: Monday, 2: Thuesday, ..., 7: Sunday]" << endl;
    cin >> weekDayInitMonth;

    cout << "\nCalendar for " << year;

    for (unsigned char month{0}; month < 12; month++)
    {
        unsigned int daysOfMonth = 0;
        cout << "\n\n-- ";
        switch (month)
        {
        case 0:
            cout << "January";
            daysOfMonth = 31;
            break;
        case 1:
            cout << "February";

            if ((year % 4) == 0)
            {
                daysOfMonth = 29;
            }
            else
            {
                daysOfMonth = 28;
            }
            break;
        case 2:
            cout << "March";
            daysOfMonth = 31;
            break;
        case 3:
            cout << "April";
            daysOfMonth = 30;
            break;
        case 4:
            cout << "May";
            daysOfMonth = 31;
            break;
        case 5:
            cout << "June";
            daysOfMonth = 30;
            break;
        case 6:
            cout << "July";
            daysOfMonth = 31;
            break;
        case 7:
            cout << "August";
            daysOfMonth = 31;
            break;
        case 8:
            cout << "September";
            daysOfMonth = 30;
            break;
        case 9:
            cout << "October";
            daysOfMonth = 31;
            break;
        case 10:
            cout << "November";
            daysOfMonth = 30;
            break;
        case 11:
            cout << "Dicember";
            daysOfMonth = 31;
            break;
        default:
            break;
        }

        cout << " --" << endl;
        cout << left << setfill(' ') << setw(SPACING) << "Mon" 
             << left << setfill(' ') << setw(SPACING) << "Tue" 
             << left << setfill(' ') << setw(SPACING) << "Wed"
             << left << setfill(' ') << setw(SPACING) << "Thu" 
             << left << setfill(' ') << setw(SPACING) << "Fri"
             << left << setfill(' ') << setw(SPACING) << "Sat"
             << left << setfill(' ') << setw(SPACING) << "Sun" << endl;

        int dayOfWeek = 0;
        int i = 0; 
        while (i < daysOfMonth)
        {
            
            if( (weekDayInitMonth - 1) > 0)
            {
                // Skip until right start week day of month
                cout << left << setfill(' ') << setw(SPACING) << " ";
                weekDayInitMonth--;
            }
            else
            {
                // Print number of the month
                cout << left << setfill(' ') << setw(SPACING) << (i + 1);
                i++;
            }

            // New line of calendar
            if (dayOfWeek == 6)
            {
                cout << endl;
                dayOfWeek = 0;
            }
            else
            {
                dayOfWeek++;
            }
        }

        // Calculate the start week day of next month
        if (dayOfWeek > 0)
        {
            weekDayInitMonth = dayOfWeek + 1;
        }
        else
        {
            weekDayInitMonth = 1;
        }
        
    }

    cout << endl;

    return 0;
}