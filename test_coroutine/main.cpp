#include <coroutine>
#include <iostream>

using namespace std;

coro[int] func1() {
	co_yield 45;
	co_yield 46;
	co_yield 47;
	co_return 48;
}

int main () {
	auto f1 = func1();	//-> f1 si può considerarlo come handler per il calcolo
						// 
	cout << f1() << endl; //45
	cout<< f1() <<	endl;//46
	cout<< f1() <<	endl;//47
	cout<< f1() <<	endl;//48
	cout<< f1() << endl;	//UB la funzione cede 3 numneri e noi vogliamo
	
	return 0;
}