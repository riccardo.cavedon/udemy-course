#include <iostream>

using namespace std;

int main(){
    int age{0};
    cout << "Please type in your age : " << endl;
    cin>> age;

    if(age < 21)
    {
        cout << "Sorry, you are too young for treatment" << endl;
    }
    else if(age > 39)
    {
        cout << "Sorry, you are too old for treatment" << endl;
    }
    else
    {
        cout << "You are eligible for the treatment" << endl;
    }

    return 0;
}