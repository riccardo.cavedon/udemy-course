#include "scores.h"
#include <cassert>

double& Scores::operator[](size_t index) {
    assert( (index >= 0) && (index < 20)); //Proteggere dagli indici sbagliati
    return m_scores[index];
}

double Scores::operator[](size_t index) const { //--> passo in cost ma ritorno per valore
    assert( (index >= 0) && (index < 20));
    return m_scores[index];
}