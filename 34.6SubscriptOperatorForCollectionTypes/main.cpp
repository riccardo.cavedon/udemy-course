#include <iostream>
#include "scores.h"

int main(){
    Scores math("math");
    math.print_info();

    math[5] = 66.4;

    for (size_t i = 0; i < 20; i++) {
        if(i == 0) {
            math[0] = 73.7;
        }
        else {
            math[i] = math[i - 1] + 0.56;
        }
    }
    

    math.print_info();

    std::cout << "-------------------" << std::endl;

    // Const object
    const Scores geo("Geography");
    std::cout << "geo  [5] : " << geo[5] << std:: endl; // errore perché operator non prevede un const object!!!! va fatto un'ulteriore oveloading

    //geo[4] = 55.8; // errore perché cerco di modificare un'oggetto costante

    return 0;
}