#include <iostream>


int main(){
    double value = 0.0;
    std::cout << "Please enter a degree value in Celsius :" << std::endl;
    std::cin >> value;

    double fahrenheit = (9.0/5.0)* value + 32;

    std::cout << value << " Celsius is " << fahrenheit << " Fahrenheit" << std::endl;
    return 0;
}