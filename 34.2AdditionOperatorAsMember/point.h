#ifndef POINT_H
#define POINT_H
#include <iostream>

class Point
{
public:
	Point() = default;
	Point(double x, double y) : 
		m_x(x), m_y(y){
	}
	~Point() = default;

    // Member
    /*
    Point operator+(const Point& right_hand) const {
        return Point(m_x + right_hand.m_x, 
                        m_y + right_hand.m_y);
    }*/

    Point operator+(const Point& right_hand);

	void print_info(){
		std::cout << "Point [ x : " << m_x << ", y : " << m_y << "]" << std::endl;
	}
private: 
	double length() const;   // Function to calculate distance from the point(0,0)

private : 
	double m_x{};
	double m_y{};
};

#endif // POINT_H