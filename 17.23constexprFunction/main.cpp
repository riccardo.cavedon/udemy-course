#include <iostream>

constexpr int get_value(int multiplier){
    return 3 * multiplier;
}

int main(){
    //int result = get_value(4); //--> compile time
    //std::cout << "result : " << result << std::endl;

    int some_var{5};
    int result = get_value(some_var);//--> runtime perchè argomento è una variabile runtime
    std::cout << "result : " << result << std::endl;

    return 0;
}